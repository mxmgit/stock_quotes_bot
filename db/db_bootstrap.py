import logging
import logging.config
import sqlite3
from sqlite3 import Error

from config import config

logging.config.fileConfig('config/logging.conf')
logger = logging.getLogger("info")

def init_db():
    try:
        connection = get_db_connection()
        cursor = connection.cursor()
        cursor.executescript(''' 
                CREATE TABLE IF NOT EXISTS symbols(
                    symbol TEXT PRIMARY KEY NOT NULL,
                    name TEXT NOT NULL,
                    currency TEXT NOT NULL,
                    FOREIGN KEY(name) REFERENCES symbols_name(name),
                    FOREIGN KEY(currency) REFERENCES currencies(name));

                CREATE TABLE IF NOT EXISTS currencies(
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    name TEXT UNIQUE NOT NULL);

                CREATE TABLE IF NOT EXISTS symbols_name(
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    name TEXT UNIQUE NOT NULL);
                ''')
        connection.commit()
        connection.close()
    except Error as e:
        logger.info('DB_INIT_ERROR: %s' % (e))
        pass

def get_db_connection():
    try:
        return sqlite3.connect(config.db_path)
    except Error as e:
        logger.info('DB_GET_CONNECTION_ERROR: ' % (e))
        pass
