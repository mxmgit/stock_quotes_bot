import logging
import logging.config

import telebot
from telebot import types
from service import service
from config import config

from db import db_bootstrap as db_bootstrap
from service import db_queries as dbq

logging.config.fileConfig('config/logging.conf')
logger = logging.getLogger("info")

bot = telebot.TeleBot(config.bot_token)

data = {}


@bot.inline_handler(lambda query: len(query.query) > 1)
def query_text(query):
    ticker = query.query.upper()
    try:
        filtered_dict = service.get_filtered_tickers_dict(ticker, data)
        tickers_list = []
        for key in filtered_dict.keys():
            tickers_list.append(types.InlineQueryResultArticle(
                id=key, title=data[key],
                description="{!s}".format(key),
                input_message_content=types.InputTextMessageContent(
                message_text=filtered_dict[key])))
        bot.answer_inline_query(query.id, tickers_list)
    except Exception as e:
        logger.info('ERROR_query_text: "%s"' % (e))
        pass



if __name__ == '__main__':
    data = dbq.fetch_all_symbols_dict()
    logger.info('bot started, data loaded')
    bot.infinity_polling()







