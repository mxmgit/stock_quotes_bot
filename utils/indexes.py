
ru_idxs = {'MOEXBMI.ME': 'Moscow Exchange Broad Market In', 'MOEXCH.ME': 'MICEX Chemicals Index', 'MOEXCN.ME': 'MICEX Consumer Goods and Servic', 'MOEXEU.ME': 'MICEX Power Index', 'MOEXFN.ME': 'MICEX Financials Index', 'MOEXINN.ME': 'MICEX Innovation Index', 'MOEXMM.ME': 'MICEX Metals and Mining Index', 'IMOEX.ME': 'MOEX Russia Index', 'MCXSM.ME': 'MOEX SMID Index', 'MOEXTL.ME': 'MICEX Telecommunication Index', 'MOEXTN.ME': 'Moscow Exchange Transport Index', 'RUBMI.ME': 'RTS Broad market Index', 'RTSCH.ME': 'RTS Chemicals Index', 'RTSI.ME': 'RTS Index'}

def get_idxs():
    return ru_idxs
