from stocksymbol import StockSymbol
import yahooquery as yq
import json

import config
import currencies
import commex
import indexes
import db_bootstrap as db



symbols_api = StockSymbol(config.symbols_api_key)

curr_data = currencies.get_curr_data()
comex_symbols = commex.get_commex_symbols()

def get_symbols_data(tickers_dict):
    res = {}
    requested_symbols = ' '.join([str(item) for item in list(tickers_dict)])
    tickers = yq.Ticker(requested_symbols)
    data = tickers.quotes
    try:
        for key in data.keys():
            if 'regularMarketPrice' in data[key]:
                sub_data = {}
                sub_data['currency'] = data[key]['currency']
                sub_data['name'] = tickers_dict[key]
                res[key] = sub_data
        return res
    except Exception as e:
        print('ERROR_get_symbols_data: "%s"' % (e))
        pass


def get_tickers_dict():
    markets_list = ['us', 'ru']
    tickers_dict = {}
    try:
        for key, value in comex_symbols.items():
            tickers_dict[key] = value
        for market in markets_list:
            symbol_list = symbols_api.get_symbol_list(market=market)
            for item in symbol_list:
                tickers_dict[item['symbol']] = item['longName']
        for item in get_crypto_symbols():
            tickers_dict[item['symbol']] = item['shortName']
        for key, value in get_curr_symbols().items():
            tickers_dict[key] = value
        for key,value in indexes.get_idxs().items():
            tickers_dict[key] = value
    except Exception as e:
        print('ERROR_get_tickers_dict: "%s"' % (e))
    return tickers_dict


def get_crypto_symbols():
    try:
        screener = yq.Screener()
        data = screener.get_screeners('all_cryptocurrencies_us', count=250)
        dicts = data['all_cryptocurrencies_us']['quotes']
    except Exception as e:
        print('ERROR_get_crypto_symbols: "%s"' % (e))
    return list(dicts)
 

def get_curr_symbols():
    try:
        data = yq.get_currencies()
        curr_dict = {}
        for curr in data:
            symbol = curr['symbol'] + '=X'
            curr_dict[symbol] = curr['longName'] + '/USD'
            symbol = curr['symbol'] + 'USD=X'
            curr_dict[symbol] = 'USD/' + curr['longName']
            symbol = curr['symbol'] + 'RUB=X'
            curr_dict[symbol] = curr['longName'] + '/RUB'
        return curr_dict
    except Exception as e:
        print('ERROR_get_curr_symbols: "%s"' % (e))


def fill_out_db():
    db.init_db()
    data = get_tickers_dict()
    res = get_symbols_data(data)
    for key in res.items():
        symbol = key[0]
        name = key[1]['name']
        curr = key[1]['currency']
        #if symbol.upper()=='AAP':
        print('symbol: ', symbol, 'name: ', name, 'currency: ', curr)
        db.insert_into_symbols_name(name)
        db.insert_into_currencies(curr)
        db.insert_into_symbols(symbol, name, curr)
    print('ready')


def get_ticker_info(ticker):
    tickers = yq.Ticker(ticker)
    return tickers.quotes



if __name__ == "__main__":
    fill_out_db()
    #data = get_ticker_info('FIXP.ME')
    #print(json.dumps(data, sort_keys=True, indent=4))
    

