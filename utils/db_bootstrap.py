import sqlite3
from sqlite3 import Error

import config

def init_db():
    try:
        connection = get_db_connection()
        cursor = connection.cursor()
        cursor.executescript(''' 
                CREATE TABLE IF NOT EXISTS symbols(
                    symbol TEXT PRIMARY KEY NOT NULL,
                    name TEXT NOT NULL,
                    currency TEXT NOT NULL,
                    FOREIGN KEY(name) REFERENCES symbols_name(name),
                    FOREIGN KEY(currency) REFERENCES currencies(name));

                CREATE TABLE IF NOT EXISTS currencies(
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    name TEXT UNIQUE NOT NULL);

                CREATE TABLE IF NOT EXISTS symbols_name(
                    id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
                    name TEXT UNIQUE NOT NULL);
                ''')
        connection.commit()
        connection.close()
    except Error as e:
        print(f"Init_DB error '{e}' occured")
        pass

def get_db_connection():
    try:
        return sqlite3.connect(config.db_path)
    except Error as e:
        print('DB connection erorr: ', e)


def insert_into_symbols_name(name):
    try:
        connection = get_db_connection()
        cursor = connection.cursor()
        data = (name,)
        sql = 'INSERT INTO symbols_name(name) VALUES(?)'
        cursor.execute(sql, data)
        connection.commit()
        connection.close()
    except Error as e:
        print('DB_INSERT_INTO_SYMBOLS_NAME_Error - "%s"' % (e))
        pass

def insert_into_currencies(currency):
    try:
        connection = get_db_connection()
        cursor = connection.cursor()
        data = (currency,)
        sql = 'INSERT INTO currencies(name) VALUES(?)'
        cursor.execute(sql, data)
        connection.commit()
        connection.close()
    except Error as e:
        print('DB_INSERT_INTO_CURRENCIES_Error - "%s"' % (e))
        pass

def insert_into_symbols(symbol, name, currency):
    try:
        connection = get_db_connection()
        cursor = connection.cursor()
        data = (symbol, name, currency)
        sql = 'INSERT INTO symbols(symbol, name, currency) VALUES(?,?,?)'
        cursor.execute(sql, data)
        connection.commit()
        connection.close()
    except Error as e:
        print('DB_INSERT_INTO_SYMBOLS_Error - "%s"' % (e))
        pass
