import logging
import logging.config

from stocksymbol import StockSymbol
import yahooquery as yq

from config import config
from utils import currencies
from service import db_queries as dbq


logging.config.fileConfig('config/logging.conf')
logger = logging.getLogger("info")

symbols_api = StockSymbol(config.symbols_api_key)

curr_data = currencies.get_curr_data()

def get_symbols_data(tickers_dict):
    result = {}
    requested_symbols = ' '.join([str(item) for item in list(tickers_dict)])
    tickers = yq.Ticker(requested_symbols)
    data = tickers.quotes
    try:
        for key in data.keys():
            if 'regularMarketPrice' in data[key]:
                curr = data[key]['currency']

                # get currency symbol sign
                currency = curr_data[curr]['sign'] if curr_data[curr]['sign'] != None else '$'
                trend = '🔴' if (data[key]['regularMarketChangePercent'] < 0) else '🟢'

                response = '#{0}\n{1}\n\n{2} {3}{4:.2f}  ({5:.2f}%)\nToday: 🔼{3}{6:.2f}  🔽{3}{7:.2f}\n52 wk: 🔼{3}{8:.2f}  🔽{3}{9:.2f}'
                result[key] = response.format(
                        key,                                        # symbol
                        tickers_dict[key],                          # company name or currency name
                        trend,                                      # trend
                        currency,                                   # ticker currency
                        data[key]['regularMarketPrice'],            # current price
                        data[key]['regularMarketChangePercent'],    # price change in percent
                        data[key]['regularMarketDayHigh'],          # daily price high
                        data[key]['regularMarketDayLow'],           # daily price low
                        data[key]['fiftyTwoWeekHigh'],              # 52 weeks price high
                        data[key]['fiftyTwoWeekLow']                # 52 weeks price low
                        )
        return result
    except Exception as e:
        logger.info('ERROR_get_symbols_data: "%s"' % (e))
        pass

def get_filtered_tickers_dict(ticker, data):
    res={}
    for key,value in data.items():
        if ((ticker.upper() in value.upper()) or(ticker.upper() in key.upper())):
            res[key] = data[key]
    return get_symbols_data({k: data[k] for k in list(res)[:20]})



