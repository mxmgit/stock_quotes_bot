import sqlite3
from sqlite3 import Error
import logging

from db import db_bootstrap as db

logger = logging.getLogger("info")



def insert_into_symbols_name(name):
    try:
        connection = db.get_db_connection()
        cursor = connection.cursor()
        data = (name,)
        sql = 'INSERT INTO symbols_name(name) VALUES(?)'
        cursor.execute(sql, data)
        connection.commit()
        connection.close()
    except Error as e:
        logger.error('DB_INSERT_INTO_SYMBOLS_NAME_Error - "%s"' % (e))
        pass

def insert_into_currencies(currency):
    try:
        connection = db.get_db_connection()
        cursor = connection.cursor()
        data = (currency,)
        sql = 'INSERT INTO currencies(name) VALUES(?)'
        cursor.execute(sql, data)
        connection.commit()
        connection.close()
    except Error as e:
        logger.error('DB_INSERT_INTO_CURRENCIES_Error - "%s"' % (e))
        pass

def insert_into_symbols(symbol, name, currency):
    try:
        connection = db.get_db_connection()
        cursor = connection.cursor()
        data = (symbol, name, currency)
        sql = 'INSERT INTO symbols(symbol, name, currency) VALUES(?,?,?)'
        cursor.execute(sql, data)
        connection.commit()
        connection.close()
    except Error as e:
        logger.error('DB_INSERT_INTO_SYMBOLS_Error - "%s"' % (e))
        pass

def fetch_all_symbols_dict():
    try:
        connection = db.get_db_connection()
        cursor = connection.cursor()
        sql = "SELECT * FROM symbols"
        result = {line[0]: line[1] for line in connection.execute(sql)}
        connection.commit()
        connection.close()
        return result
    except Error as e:
        logger.error('DB_FETCH_ALL_SYMBOLS_Error - "%s"' % (e))
        pass

